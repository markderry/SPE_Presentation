# R Course 1

# basic operations
# Math
1+2
2*2
3^2

# Objects
# Variables
x <-1
y <- 2+x
z=c(x, y)

# data types
class(1)
class("a")
class(c("a", "b"))

# functions
sin(.5)
x <-8
sqrt(x)

# create your own funciton
myfunction <- function(x,y){x+y^2}
# use that function
myfunction(5,4)


# Looping
for(i in 1:10){print(i)}

# use a function in a loop
for(i in 1:10){print(myfunction(5,i))}

# Packages
# A package is just a collection of functions
# Packages need to be installed from a Package Repository
# you can use the function 
install.packages("Quandl")
# Or use Tools > Install Packages...

# Then you need to call the Pacakgae, this is done using the "library" function
library(Quandl)

# once this is loaded you can use the functions inside
# Use this to search the Quandl database
Quandl.search("OPEC")
# https://www.quandl.com/data/OPEC/ORB-OPEC-Crude-Oil-Price

# Use this to load price data for OPEC
Opec_prices <- Quandl("OPEC/ORB")

# Lets rename columns
head(Opec_prices)
setnames(Opec_prices, "Value", "Oil_Price")
head(Opec_prices)

# Check Class of Date Column
class(Opec_prices$Date)

# calculate average price over entire data set
mean(Opec_prices$Oil_Price)

# get max and min year
max(year(Opec_prices$Date))
min(year(Opec_prices$Date))

# if we want to do these calculations in a more elegant way we will need a new package
# data.table package
install.packages("data.table")
library(data.table)
Opec_prices_dt <- data.table(Opec_prices)

# data.table lets us do more elegant filtering and calculations
# Filter the data to just 2014
Opec_prices_dt[ year(Date)==2014, ]

# add a column
Opec_prices_dt[, Year := year(Date)]
Opec_prices_dt[, Month := month(Date)]
Opec_prices_dt[, Calc_example := Oil_Price/10]

# use the by operator
Opec_prices_dt[, monthly_avg_price:=mean(Oil_Price), by="Month"]

# What is wrong with the monthly_avg_price?
# Assignment: fix monthly_avg_price so it reflects the correct monthly average


# Load CSV file of OS production data
OS_Production <- read.csv(file= "/ressim_data_calgary/Analytics/Shared_R/Training/OS_Production_Daily_Data.csv", header = TRUE, stringsAsFactors = FALSE)

# Access Columns, Rows or Values
OS_Production[1,]
OS_Production[1, 1:10]
OS_Production[1:10,1]
OS_Production[1:10,1:6]

# Rearrange tables
# First change table to a data.table format
OS_Production_dt <- data.table(OS_Production)

# Check Class of date
class(OS_Production$PRDDATE[1])
class(OS_Production_dt$PRDDATE[1])

# Add a date column that is a date class
# There are many ways to do this here are two examples
# non data.table way
OS_Production$DATE <- as.Date(OS_Production$PRDDATE)
head(OS_Production)
class(OS_Production$DATE[1])
# OR using data.table
OS_Production_dt[, DATE:=as.Date(PRDDATE)]
OS_Production_dt
class(OS_Production_dt$DATE[1])

## Assignment 
# 1) Replace character PRDDATE column with date column

# Trim Table
OS_Prd_sm <- OS_Production_dt[, list(SURFACE_PAD,WELL_PAIR_ID, PRDDATE, PRO_OIL, PRO_WATER, STEAM_INJ)]

# Trim table to just 6 colums and a calculation
OS_Prd_sm <- OS_Production_dt[, list(SURFACE_PAD,WELL_PAIR_ID, PRDDATE, PRO_OIL, PRO_WATER, STEAM_INJ, SOR=STEAM_INJ/PRO_OIL)]


browseURL("https://www.quandl.com/data/JODI/OIL_CRPRKT_OMN-Oil-Flows-Crude-Oil-Production-Oman", browser = getOption("browser"))
# see: https://www.quandl.com/data/JODI/OIL_CRPRKT_OMN-Oil-Flows-Crude-Oil-Production-Oman
Oman_Production <- Quandl("JODI/OIL_CRPRKT_OMN")

# Assignment: 
# 1) Turn Oman_Prod into a data.table object
# 2) Calculate the yearly total production

Oman_Production_dt <- data.table(Oman_Production)
Oman_Production_dt[, Year:=year(as.Date(Date))]
Yearly_Prd <- Oman_Production_dt[, sum(Value), by="Year"]


# writing files
write.csv(x = OS_Prd_sm, file = "~R/OS_Production_Daily_Data_sm.csv")


#### Home work #####
# https://www.datacamp.com/courses/data-table-data-manipulation-r-tutorial
browseURL("https://www.datacamp.com/courses/data-table-data-manipulation-r-tutorial", browser = getOption("browser"))

#### Home work #####
# install "swirl" via "Tools > Install Packages..."
library(swirl)
install_from_swirl("R Programming")
swirl()



# Plotting
plot(Opec_prices)

# ggplot plotting
library(ggplot2)
a <- ggplot(mpg, aes(hwy))
a
a + geom_area(stat = "bin")
a + geom_density(kernel = "gaussian")
a + geom_dotplot()






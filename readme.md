
# Improving Efficiency With Big Data & Analytics
Author: Mark Derry

Date: 2018-02-23

## Background

Here is the [Presentation Link](http://prezi.com/inph2xvq9sch/?utm_campaign=share&utm_medium=copy).

I believe that all the work that we do should be repeatable and shareable and as such, for this presentation I am including the code associated with the examples I presented.
